# Recognize Any Artwork AI
**High-Performance CPU Inference for Artwork Recognition**

## Overview

**Recognize Any Artwork AI** is an AI-powered system designed to recognize artworks from mobile phone images with high accuracy and efficiency. Inspired by the **Humpback Whale Identification Kaggle Competition**, this project adapts state-of-the-art recognition techniques to the domain of artwork authentication.

Developed with **fast.ai (v1.0.61)** and **PyTorch**, the model achieves over **95% accuracy** while running entirely on CPU. The system was used in production for **Artshield**, where it powered real-world artwork authentication via a **WeChat Mini Program** in China.

## Features

✅ **CPU-Optimized Inference** – Uses pickled weights for **sub-seconds recognition** without GPUs.  
✅ **Inspired by Top AI Research** – Adapted from a [top 10 Kaggle repo](https://github.com/radekosmulski/whale) of the **Humpback Whale Identification Competition**.  
✅ **Data Augmentation** – Utilizes **fast.ai** techniques to expand datasets and improve generalization across different art styles.  
✅ **Jupyter Notebook-Based** – The project is fully developed in **Jupyter Notebooks**, making it interactive and easy to experiment with.  
✅ **Real-World Usage** – Previously deployed in production for **Artshield**, processing artwork recognition requests through a **WeChat Mini Program**.

## Technologies Used

| Category         | Tools & Libraries |
|-----------------|------------------|
| **Model Training** | Jupyter Notebook, PyTorch, fast.ai |
| **Image Processing** | OpenCV, Pillow |
| **Inference Optimization** | Pickle (for in-memory model loading) |
| **Visualization & Evaluation** | Matplotlib, Scikit-learn |

## How to Use

Since this project is **developed in Jupyter Notebooks**, you’ll need to run it in an interactive environment.

### 1️⃣ Installation
Clone the repository and install dependencies:
```bash
git clone https://gitlab.com/xevimacia/recognize-any-artwork-ai.git
cd recognize-any-artwork-ai
```
### 2️⃣ Launch Jupyter Notebook
Ensure you have Jupyter installed:
```bash
pip install jupyterlab
```
Then start Jupyter:
```bash
jupyter lab
```
Open and execute the notebooks in the correct sequence (0.ipynb → 6.ipynb).

### 3️⃣ Train the Model
Run all the cells in 0.ipynb → 4.ipynb to train the model and save the weights.

### 4️⃣ Run Inference
To test the model on a new image, open 6.ipynb → 6.ipynb, upload an image, and run the notebook cells.

## Challenges & Learnings
Building this system required overcoming several technical hurdles:

- **Domain Adaptation** – Transitioning from **whale identification** to **artwork recognition** required adjusting dataset preprocessing and feature extraction.
- **CPU Optimization** – Instead of GPU inference, the model loads into memory via **pickled weights**, enabling **fast CPU-based processing**.
- **Data Scarcity** – Given limited labeled artwork data, the dataset was **augmented using [fast.ai](https://www.fast.ai/) techniques** to enhance model generalization.

## Case Study
A case study with a video showcasing the inference model working in real-time can be found [here](https://xevi.work/work/recognize-any-artwork-ai/)

## Acknowledgments
This project is inspired by the **Humpback Whale Identification Kaggle Competition** and a [top 10 Kaggle repository](https://github.com/radekosmulski/whale). Special thanks to **Radek Osmulski and Dmytro Mishkin** for their foundational research.

